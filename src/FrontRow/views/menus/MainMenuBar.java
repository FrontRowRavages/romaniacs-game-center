package FrontRow.views.menus;

import FrontRow.GameStoreCPH;
import FrontRow.views.dialogs.LogInDialog;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * Created by chill on 4/25/16.
 */
public class MainMenuBar {

    private Stage activeStage;

    public MenuBar mainMenuBar(){
        activeStage = new Stage();
        MenuBar mb = new MenuBar();
        Menu file = new Menu("File");
        MenuItem logout = new MenuItem("Logout");
        MenuItem exit = new MenuItem("Exit");

        Menu view = new Menu("View");
        //MenuItem viewCustomers = new MenuItem("View Customers");
        //MenuItem viewGames = new MenuItem("View Games");

        file.getItems().addAll(logout, exit);
        //view.getItems().addAll(viewCustomers, viewGames);
        mb.getMenus().addAll(file, view);

        // Listeners //
        logout.setOnAction(event -> {

            activeStage = new Stage();
            activeStage = GameStoreCPH.getMainStage();
            activeStage.close();

            LogInDialog logInDialog = new LogInDialog();
            logInDialog.viewLogInScreen();

        });

        exit.setOnAction(event -> {
            activeStage = GameStoreCPH.getMainStage();
            activeStage.close();
        });

        return mb;
    }

}
